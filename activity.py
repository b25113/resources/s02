leapYear = input("Please enter a year:\n")
result = int(leapYear)

if leapYear.isdigit():
	if int(leapYear) <= 0:
		if (int(leapYear) % 4) == 0:
			print(f"{leapYear} is a leap year")
		else:
			print(f"{leapYear} is not a leap year")
else:
	print("You've entered a non-numeric value")

row = int(input("Enter num for row:\n"))
col = int(input("Enter num for column:\n"))
x = 1
y = 0

for x in range(row):
	print("\n")
	for y in range(col):
		print("* ",end="")
